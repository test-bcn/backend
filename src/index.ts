import { configDotenv } from "dotenv";
import { Server } from "./infrastructure/config/server";

(() => {
  try {
    configDotenv();
    const server = new Server();
    server.init().listen(
      process.env.PORT,
      () => console.log(`Server running on port ${process.env.PORT}`)
    );
  } catch (err) {
    console.log(err);
  }
})();
