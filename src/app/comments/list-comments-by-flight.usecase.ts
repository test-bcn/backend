import { ICommentsRepository } from "../../domain/comments/comment-repository.interface";
import { IUseCaseCommon } from "../common/usecase.interface";

export class ListCommentsByFlightUseCase implements IUseCaseCommon {
  private readonly commentRepository: ICommentsRepository;

  constructor(commentRepository: ICommentsRepository) {
    this.commentRepository = commentRepository;
  }

  async execute(flight_id: number) {
    return await this.commentRepository.filterByFlight(flight_id);
  }
}
