import { ICommentsRepository } from "../../domain/comments/comment-repository.interface";
import { IUseCaseCommon } from "../common/usecase.interface";

export class ListCommentsUseCase implements IUseCaseCommon {
  private readonly commentRepository: ICommentsRepository;

  constructor(commentRepository: ICommentsRepository) {
    this.commentRepository = commentRepository;
  }

  async execute() {
    return await this.commentRepository.index();
  }
}
