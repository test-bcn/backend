import { ICommentsRepository } from "../../domain/comments/comment-repository.interface";
import { CommentDomain } from "../../domain/comments/comment.domain";
import { IUseCaseCommon } from "../common/usecase.interface";

export class CreateCommentUseCase implements IUseCaseCommon {
  private readonly commentRepository: ICommentsRepository;

  constructor(commentRepository: ICommentsRepository) {
    this.commentRepository = commentRepository;
  }

  async execute(comment: CommentDomain) {
    this.validate(comment);
    return await this.commentRepository.create(comment);
  }

  private validate(registro: CommentDomain) {
    return CommentDomain.validate(registro);
  }
}
