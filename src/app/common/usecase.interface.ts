export interface IUseCaseCommon {
  execute(data?: any): Promise<any>;
}
