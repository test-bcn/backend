import { IUsersRepository } from "../../domain/users/user-repository.interface";
import { IUseCaseCommon } from "../common/usecase.interface";

export class ListUsersUseCase implements IUseCaseCommon {
  private readonly userRepository: IUsersRepository;

  constructor(userRepository: IUsersRepository) {
    this.userRepository = userRepository;
  }

  async execute() {
    return await this.userRepository.index();
  }
}
