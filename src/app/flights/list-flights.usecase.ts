import { IFlightsRepository } from "../../domain/flights/flight-repository.interface";
import { IUseCaseCommon } from "../common/usecase.interface";

export class ListFlightsUseCase implements IUseCaseCommon {
  private readonly flightRepository: IFlightsRepository;

  constructor(userRepository: IFlightsRepository) {
    this.flightRepository = userRepository;
  }

  async execute() {
    return await this.flightRepository.index();
  }
}
