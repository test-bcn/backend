import { FlightDomain } from "./flight.domain";

export interface IFlightsRepository {
  index(): Promise<FlightDomain[]>;
}
