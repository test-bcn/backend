export class FlightDomain {
  private id!: number;
  private title!: string;

  constructor({ id, title }: { id: number; title: string }) {
    this.id = id;
    this.title = title;
  }
}
