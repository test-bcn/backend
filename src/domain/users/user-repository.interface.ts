import { UserDomain } from "./user.domain";

export interface IUsersRepository {
  index(): Promise<UserDomain[]>;
}
