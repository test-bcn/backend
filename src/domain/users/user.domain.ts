export class UserDomain {
  id!: number;
  name!: string;
  created_at!: Date;

  constructor({
    id,
    name,
    created_at,
  }: {
    id: number;
    name: string;
    created_at?: string;
  }) {
    this.id = id;
    this.name = name;
    this.created_at = created_at ? new Date(created_at) : new Date();
  }
}
