import { CommentDomain } from "./comment.domain";

export interface ICommentsRepository {
  index(): Promise<CommentDomain[]>;
  filterByFlight(flight_id: number): Promise<CommentDomain[]>;
  create(comment: CommentDomain): Promise<CommentDomain>;
}
