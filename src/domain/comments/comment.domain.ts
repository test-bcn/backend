import Joi from "joi";
import { ValidationError } from "../../infrastructure/shared/errors";
import { UserDomain } from "../users/user.domain";

export class CommentDomain {
  id?: number;
  content!: string;
  user_id!: number;
  flight_id!: number;
  tags!: string[];
  created_at!: Date;
  user?: UserDomain;

  constructor({
    id,
    content,
    user_id,
    flight_id,
    tags,
    created_at,
    user,
  }: {
    id: number;
    content: string;
    user_id: number;
    flight_id: number;
    tags: string[];
    created_at?: string;
    user?: {
      id: number;
      name: string;
      created_at?: string;
    };
  }) {
    this.id = id;
    this.content = content;
    this.user_id = user_id;
    this.flight_id = flight_id;
    this.tags = tags;
    this.created_at = created_at ? new Date(created_at) : new Date();
    this.user = user
      ? new UserDomain(user)
      : new UserDomain({ id: 0, name: "" });
  }

  static validate(data: CommentDomain): CommentDomain {
    const schema = Joi.object({
      content: Joi.string().required(),
      user_id: Joi.number().positive().required(),
      flight_id: Joi.number().positive().required(),
      tags: Joi.array().items(Joi.string()).optional(),
    });
    const result = schema.validate(data);

    if (result.error) {
      throw new ValidationError(result.error?.message);
    }

    return new CommentDomain({
      id: data.id ?? 0,
      content: data.content,
      user_id: data.user_id,
      flight_id: data.flight_id,
      tags: data.tags,
    });
  }
}
