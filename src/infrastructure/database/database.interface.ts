export interface IDatabase {
  create(query: string, data: object): Promise<number>;
  getAll(query: string, data: object): Promise<any[]>;
  update(query: string, data: object, id: number): Promise<number>;
  delete(query: string, id: number): Promise<number>;
}
