import Db, { DbConfig } from "mysql2-async";
import { IDatabase } from "./database.interface";
import { PersistenceError } from "../shared/errors";

export class MysqlDatabase implements IDatabase {
  private config!: DbConfig;
  private connection!: Db;

  private configDatabase() {
    this.config = {
      host: process.env.DB_HOST ?? "localhost",
      port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 3306,
      user: process.env.DB_USER ?? "root",
      password: process.env.DB_PASSWORD ?? "",
      database: process.env.DB_DATABASE ?? "test_db",
      skiptzfix: true,
    };
  }

  private connect() {
    this.configDatabase();
    this.connection = new Db(this.config);
  }

  public async getAll(query: string, dataFilter: object): Promise<any[]> {
    try {
      this.connect();
      const data = Object.values(dataFilter).map((value) => value);
      return await this.connection.getall(query, data);
    } catch (err) {
      console.log(err);
      throw new PersistenceError("Error getting data from database");
    }
  }

  public async create(query: string, entity: object): Promise<number> {
    try {
      this.connect();
      const data = Object.values(entity).map((value) => value);
      return await this.connection.insert(query, data);
    } catch (err) {
      console.log(err);
      throw new PersistenceError("Error creating data in database");
    }
  }

  public async update(
    query: string,
    entity: object,
    id: number
  ): Promise<number> {
    try {
      this.connect();
      const data = Object.values(entity).map((value) => value);
      return await this.connection.update(query, [...data, id]);
    } catch (err) {
      console.log(err);
      throw new PersistenceError("Error updating data in database");
    }
  }

  public async delete(query: string, id: number): Promise<number> {
    try {
      this.connect();
      return await this.connection.delete(query, [id]);
    } catch (err) {
      console.log(err);
      throw new PersistenceError("Error deleting data in database");
    }
  }
}
