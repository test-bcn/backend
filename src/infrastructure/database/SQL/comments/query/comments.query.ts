export const commentsQuery = {
  index: "SELECT * FROM comments",
  filterByFlight: `SELECT 
    com.id as com_id,
    com.user_id as com_user_id,
    com.flight_id as com_flight_id,
    com.content as com_content,
    com.tags as com_tags,
    com.created_at as com_created_at,
    u.id as user_id,
    u.name as user_name,
    u.created_at as user_created_at
    FROM 
    comments com INNER JOIN users u ON com.user_id = u.id
    WHERE
    flight_id = ?`,
  create:
    "INSERT INTO comments (user_id,flight_id, content, tags) VALUES (?, ?, ?, ?)",
  update:
    "UPDATE comments SET user_id = ?, flight_id = ?, content = ?, tags=? WHERE id = ?",
  delete: "DELETE FROM comments WHERE id = ?",
};
