import { ICommentsRepository } from "../../../../../domain/comments/comment-repository.interface";
import { CommentDomain } from "../../../../../domain/comments/comment.domain";
import { IDatabase } from "../../../database.interface";
import { commentsQuery } from "../query/comments.query";

export class CommentsRepository implements ICommentsRepository {
  constructor(private connection: IDatabase) {
    this.connection = connection;
  }

  async index() {
    const data = await this.connection.getAll(commentsQuery.index, {});
    return data.map((item) => ({
      ...item,
      tags: item.tags?.split(",") || [],
    })) as CommentDomain[];
  }

  async filterByFlight(flight_id: number) {
    const data = await this.connection.getAll(commentsQuery.filterByFlight, {
      flight_id,
    });
    return data.map(
      (item) =>
        new CommentDomain({
          id: item.com_id,
          user_id: item.com_user_id,
          flight_id: item.com_flight_id,
          content: item.com_content,
          created_at: item.com_created_at,
          tags: item.com_tags?.split(",") || [],
          user: {
            id: item.user_id,
            name: item.user_name,
            created_at: item.user_created_at,
          },
        })
    );
  }

  async create(comment: CommentDomain) {
    const result = await this.connection.create(commentsQuery.create, {
      user_id: comment.user_id,
      flight_id: comment.flight_id,
      content: comment.content,
      tags: comment.tags.join(","),
    });
    comment.id = result;
    return comment;
  }
}
