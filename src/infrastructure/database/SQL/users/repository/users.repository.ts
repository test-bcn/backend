import { IDatabase } from "../../../database.interface";
import { usersQuery } from "../query/users.query";

export class UsersRepository {
  constructor(private connection: IDatabase) {
    this.connection = connection;
  }

  async index() {
    return await this.connection.getAll(usersQuery.index, {});
  }
}
