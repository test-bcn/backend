import { IDatabase } from "../../../database.interface";
import { flightsQuery } from "../query/flights.query";

export class FlightsRepository {
  constructor(private connection: IDatabase) {
    this.connection = connection;
  }

  async index() {
    return await this.connection.getAll(flightsQuery.index, {});
  }
}
