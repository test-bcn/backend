import { Router } from "express";
import { FlightController } from "../../controllers/flight.controller";

const flightsRoutes = Router();

flightsRoutes.get("/", FlightController.index);

export default flightsRoutes;
