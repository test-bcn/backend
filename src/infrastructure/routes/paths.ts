const paths = {
  users: "/users",
  flights: "/flights",
  comments: "/comments"
};

export default paths;
