import { Router } from "express";
import { CommentController } from "../../controllers/comment.controller";

const commentsRoutes = Router();

commentsRoutes.get("/", CommentController.index);
commentsRoutes.post("/", CommentController.create);
commentsRoutes.get("/flight/:flight_id", CommentController.filterByFlight);

export default commentsRoutes;
