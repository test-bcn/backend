import { Router } from "express";
import usersRoutes from "./users/users.routes";
import flightsRoutes from "./flights/flights.routes";
import commentsRoutes from "./comments/comments.routes";
import paths from "./paths";

const apiRouter = Router();

apiRouter.use(paths.users, usersRoutes);
apiRouter.use(paths.flights, flightsRoutes);
apiRouter.use(paths.comments, commentsRoutes);

export default apiRouter;
