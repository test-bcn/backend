import { Router } from "express";
import { UserController } from "../../controllers/user.controller";

const usersRoutes = Router();

usersRoutes.get("/", UserController.index);

export default usersRoutes;
