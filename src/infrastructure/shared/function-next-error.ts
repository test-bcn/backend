// clase que se usara como decorador en las funciones de los controladores que le

import { NextFunction, Request, Response } from "express";

// pasemos como parametro a la funcion next() un error
export const functionNextError = function () {
  return function (target: Function) {
    return async function (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> {
      try {
        await target(req, res, next);
      } catch (error) {
        next(error);
      }
    };
  };
};
