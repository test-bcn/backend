import { ErrorBase } from "./error-base";

export class ValidationError extends ErrorBase {
  constructor(message: string) {
    super(message, 400);
  }
}

export class PersistenceError extends ErrorBase {
  constructor(message: string) {
    super(message, 400);
  }
}
