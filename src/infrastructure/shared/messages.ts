export const UserMessages = {
  notFound: "User not found",
  created: "User created",
  updated: "User updated",
  deleted: "User deleted",
  invalid: "Invalid user",
  listed: "Users listed"
};

export const FlightMessages = {
  notFound: "Flight not found",
  created: "Flight created",
  updated: "Flight updated",
  deleted: "Flight deleted",
  invalid: "Invalid flight",
  listed: "Flights listed"
};

export const CommentMessages = {
  notFound: "Comment not found",
  created: "Comment created",
  updated: "Comment updated",
  deleted: "Comment deleted",
  invalid: "Invalid comment",
  listed: "Comments listed"
};
