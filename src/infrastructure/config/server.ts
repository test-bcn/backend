import express, { Express } from "express";
import cors from "cors";
import apiRouter from "../routes";
import morgan from "morgan";
import { ErrorBase } from "../shared/error-base";
export class Server {
  private app!: Express;

  private configServer() {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(cors());
    this.app.use(morgan("dev"));
  }

  private routes() {
    this.app.use("/api", apiRouter);
  }

  private hanleError() {
    this.app.use(
      (
        err: ErrorBase,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        console.log("custom error");
        const { status = 500, name, message } = err;
        res
          .status(status)
          .json({
            name: name ?? "Error",
            message: message ?? "Internal server error",
          })
          .end();
      }
    );
  }

  public init() {
    this.app = express();
    this.configServer();
    this.routes();
    this.hanleError();
    return this.app;
  }
}
