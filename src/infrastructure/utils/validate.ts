export const parseSpecialValue = (value: any) => {
  if (Array.isArray(value)) {
    return value.join(",");
  }
  return value;
};
