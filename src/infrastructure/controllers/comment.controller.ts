import { NextFunction, Request, Response } from "express";
import { MysqlDatabase } from "../database/mysql-database";
import { CommonResponse } from "../shared/common-reponse";
import { CommentsRepository } from "../database/SQL/comments/repository/comments.repository";
import { ListCommentsUseCase } from "../../app/comments/list-comments.usecase";
import { CommentDomain } from "../../domain/comments/comment.domain";
import { CommentMessages } from "../shared/messages";
import { CreateCommentUseCase } from "../../app/comments/create-comment.usecase";
import { ListCommentsByFlightUseCase } from "../../app/comments/list-comments-by-flight.usecase";

export class CommentController {
  static async index(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const databaseMysqlConnection = new MysqlDatabase();
      const commentRepository = new CommentsRepository(databaseMysqlConnection);

      const comments = await new ListCommentsUseCase(
        commentRepository
      ).execute();
      res.json(
        new CommonResponse<CommentDomain[]>(comments, CommentMessages.listed)
      );
    } catch (error) {
      next(error);
    }
  }

  static async create(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const databaseMysqlConnection = new MysqlDatabase();
      const commentRepository = new CommentsRepository(databaseMysqlConnection);

      const comment = await new CreateCommentUseCase(commentRepository).execute(
        req.body
      );
      res.json(
        new CommonResponse<CommentDomain>(comment, CommentMessages.created)
      );
    } catch (error) {
      next(error);
    }
  }

  static async filterByFlight(
    { params: { flight_id } }: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const databaseMysqlConnection = new MysqlDatabase();
      const commentRepository = new CommentsRepository(databaseMysqlConnection);

      const comments = await new ListCommentsByFlightUseCase(
        commentRepository
      ).execute(parseInt(flight_id));
      res.json(
        new CommonResponse<CommentDomain[]>(comments, CommentMessages.listed)
      );
    } catch (error) {
      next(error);
    }
  }
}
