import { Request, Response } from "express";
import { MysqlDatabase } from "../database/mysql-database";
import { CommonResponse } from "../shared/common-reponse";
import { FlightMessages } from "../shared/messages";
import { FlightsRepository } from "../database/SQL/flights/repository/flights.repository";
import { ListFlightsUseCase } from "../../app/flights/list-flights.usecase";
import { FlightDomain } from "../../domain/flights/flight.domain";

export class FlightController {
  static async index(req: Request, res: Response): Promise<void> {
    const databaseMysqlConnection = new MysqlDatabase();
    const userRepository = new FlightsRepository(databaseMysqlConnection);

    const flights = await (new ListFlightsUseCase(userRepository)).execute();
    res.json(new CommonResponse<FlightDomain[]>(flights, FlightMessages.listed));
  }
}
