import { Request, Response } from "express";
import { UsersRepository } from "../database/SQL/users/repository/users.repository";
import { MysqlDatabase } from "../database/mysql-database";
import { ListUsersUseCase } from "../../app/users/list-users.usecase";
import { CommonResponse } from "../shared/common-reponse";
import { UserDomain } from "../../domain/users/user.domain";
import { UserMessages } from "../shared/messages";

export class UserController {
  static async index(req: Request, res: Response): Promise<void> {
    const databaseMysqlConnection = new MysqlDatabase();
    const userRepository = new UsersRepository(databaseMysqlConnection);

    const users = await (new ListUsersUseCase(userRepository)).execute();
    res.json(new CommonResponse<UserDomain[]>(users, UserMessages.listed));
  }
}
