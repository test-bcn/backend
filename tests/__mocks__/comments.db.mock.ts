import { CommentDomain } from "../../src/domain/comments/comment.domain";

export const commentListAllMock: CommentDomain[] = [
  {
    id: 8,
    user_id: 9,
    flight_id: 5,
    content:
      "Mollit sit aliqua elit nostrud laborum laborum ipsum. Enim excepteur reprehenderit nulla voluptate cillum minim ullamco et id cupidatat dolore.",
    tags: ["cupidatat", "do"],
    created_at: new Date("2013-08-28T00:57:03.928Z"),
  },
  {
    id: 5,
    user_id: 4,
    flight_id: 5,
    content:
      "Pariatur quis excepteur culpa duis culpa ut quis deserunt. Mollit cupidatat in exercitation eu sint est excepteur aliqua sit.",
    tags: ["reprehenderit", "laborum"],
    created_at: new Date("2010-10-13T08:29:20.687Z"),
  },
  {
    id: 9,
    user_id: 10,
    flight_id: 5,
    content:
      "Lorem ea officia Lorem ipsum non veniam deserunt pariatur officia Lorem consequat. Exercitation voluptate irure nostrud consequat sit reprehenderit et consequat sint aute nisi nostrud.",
    tags: ["sint", "adipisicing"],
    created_at: new Date("2014-10-30T10:02:26.206Z"),
  },
  {
    id: 1,
    user_id: 0,
    flight_id: 5,
    content:
      "Commodo et labore mollit sit minim deserunt laborum non aute laboris consectetur reprehenderit non voluptate. Velit laborum duis culpa pariatur eiusmod duis sint ex esse id occaecat.",
    tags: ["do", "eiusmod"],
    created_at: new Date("2011-11-25T22:14:03.140Z"),
  },
  {
    id: 10,
    user_id: 4,
    flight_id: 5,
    content:
      "Anim cillum excepteur proident Lorem pariatur do. Minim id sit culpa velit eiusmod id commodo officia.",
    tags: ["qui", "culpa"],
    created_at: new Date("2013-02-05T20:00:42.181Z"),
  },
  {
    id: 0,
    user_id: 9,
    flight_id: 5,
    content:
      "Cillum mollit aliquip reprehenderit aliquip. Magna id excepteur nisi non sit exercitation esse id ullamco.",
    tags: ["consectetur", "officia"],
    created_at: new Date("2013-08-10T16:38:10.069Z"),
  },
  {
    id: 9,
    user_id: 0,
    flight_id: 5,
    content:
      "Velit officia deserunt culpa consectetur qui incididunt consequat veniam veniam nulla. Est tempor aliqua deserunt minim non ad esse.",
    tags: ["ad", "labore"],
    created_at: new Date("2010-10-03T00:58:12.588Z"),
  },
];

export const commentListAllMysqlMock: {
  id: number;
  user_id: number;
  flight_id: number;
  content: string;
  tags: string;
  created_at: Date;
}[] = [
  {
    id: 8,
    user_id: 9,
    flight_id: 5,
    content:
      "Mollit sit aliqua elit nostrud laborum laborum ipsum. Enim excepteur reprehenderit nulla voluptate cillum minim ullamco et id cupidatat dolore.",
    tags: "cupidatat,do",
    created_at: new Date("2013-08-28T00:57:03.928Z"),
  },
  {
    id: 5,
    user_id: 4,
    flight_id: 5,
    content:
      "Pariatur quis excepteur culpa duis culpa ut quis deserunt. Mollit cupidatat in exercitation eu sint est excepteur aliqua sit.",
    tags: "reprehenderit,laborum",
    created_at: new Date("2010-10-13T08:29:20.687Z"),
  },
  {
    id: 9,
    user_id: 10,
    flight_id: 5,
    content:
      "Lorem ea officia Lorem ipsum non veniam deserunt pariatur officia Lorem consequat. Exercitation voluptate irure nostrud consequat sit reprehenderit et consequat sint aute nisi nostrud.",
    tags: "sint,adipisicing",
    created_at: new Date("2014-10-30T10:02:26.206Z"),
  },
  {
    id: 1,
    user_id: 0,
    flight_id: 5,
    content:
      "Commodo et labore mollit sit minim deserunt laborum non aute laboris consectetur reprehenderit non voluptate. Velit laborum duis culpa pariatur eiusmod duis sint ex esse id occaecat.",
    tags: "do,eiusmod",
    created_at: new Date("2011-11-25T22:14:03.140Z"),
  },
  {
    id: 10,
    user_id: 4,
    flight_id: 5,
    content:
      "Anim cillum excepteur proident Lorem pariatur do. Minim id sit culpa velit eiusmod id commodo officia.",
    tags: "qui,culpa",
    created_at: new Date("2013-02-05T20:00:42.181Z"),
  },
  {
    id: 0,
    user_id: 9,
    flight_id: 5,
    content:
      "Cillum mollit aliquip reprehenderit aliquip. Magna id excepteur nisi non sit exercitation esse id ullamco.",
    tags: "consectetur,officia",
    created_at: new Date("2013-08-10T16:38:10.069Z"),
  },
  {
    id: 9,
    user_id: 0,
    flight_id: 5,
    content:
      "Velit officia deserunt culpa consectetur qui incididunt consequat veniam veniam nulla. Est tempor aliqua deserunt minim non ad esse.",
    tags: "ad,labore",
    created_at: new Date("2010-10-03T00:58:12.588Z"),
  },
];

export const commentListFilterByFlightMysqlMock: {
  com_id: number;
  com_user_id: number;
  com_flight_id: number;
  com_content: string;
  com_tags: string;
  com_created_at: string;
  user_id: number;
  user_name: string;
  user_created_at: string;
}[] = [
  {
    com_id: 8,
    com_user_id: 9,
    com_flight_id: 5,
    com_content:
      "Mollit sit aliqua elit nostrud laborum laborum ipsum. Enim excepteur reprehenderit nulla voluptate cillum minim ullamco et id cupidatat dolore.",
    com_tags: "cupidatat,do",
    com_created_at: "2013-08-28T00:57:03.928Z",
    user_id: 9,
    user_name: "Lara",
    user_created_at: "2012-11-02T00:57:03.928Z",
  },
];

export const commentListFilterByFlightMock: CommentDomain[] = [
  {
    id: 8,
    user_id: 9,
    flight_id: 5,
    content:
      "Mollit sit aliqua elit nostrud laborum laborum ipsum. Enim excepteur reprehenderit nulla voluptate cillum minim ullamco et id cupidatat dolore.",
    tags: ["cupidatat", "do"],
    created_at: new Date("2013-08-28T00:57:03.928Z"),
    user: {
      id: 9,
      name: "Lara",
      created_at: new Date("2012-11-02T00:57:03.928Z"),
    },
  },
];
