import { commentsQuery } from "../../../../../../../src/infrastructure/database/SQL/comments/query/comments.query";
import { CommentsRepository } from "../../../../../../../src/infrastructure/database/SQL/comments/repository/comments.repository";
import { MysqlDatabase } from "../../../../../../../src/infrastructure/database/mysql-database";
import {
  commentListAllMock,
  commentListAllMysqlMock,
  commentListFilterByFlightMock,
  commentListFilterByFlightMysqlMock,
} from "../../../../../../__mocks__/comments.db.mock";

jest.mock(
  "../../../../../../../src/infrastructure/database/mysql-database",
  () => ({
    MysqlDatabase: jest.fn(),
  })
);

describe("Comments Mysql Repository", () => {
  beforeEach(() => {
    jest.clearAllMocks(); // Limpia todos los mocks antes de cada prueba
  });

  it("When se lista todos los comentarios", async () => {
    const mysql = new MysqlDatabase();
    mysql.getAll = jest.fn().mockResolvedValueOnce(commentListAllMysqlMock);
    const repository = new CommentsRepository(mysql);

    const result = await repository.index();

    expect(result).toEqual(commentListAllMock);
    expect(mysql.getAll).toHaveBeenCalledTimes(1);
    expect(mysql.getAll).toHaveBeenCalledWith(commentsQuery.index, {});
  });
  it("When se lista todos los comentarios de un vuelo", async () => {
    const mysql = new MysqlDatabase();
    mysql.getAll = jest
      .fn()
      .mockResolvedValueOnce(commentListFilterByFlightMysqlMock);
    const repository = new CommentsRepository(mysql);

    const result = await repository.filterByFlight(5);
    result.forEach(({ flight_id }) => expect(flight_id).toEqual(5));
    expect(result).toEqual(commentListFilterByFlightMock);
    expect(mysql.getAll).toHaveBeenCalledTimes(1);
    expect(mysql.getAll).toHaveBeenCalledWith(commentsQuery.filterByFlight, {
      flight_id: 5,
    });
  });

  it("When se crea un comentario", async () => {
    const mysql = new MysqlDatabase();
    mysql.create = jest.fn().mockResolvedValueOnce(1);
    const repository = new CommentsRepository(mysql);

    const result = await repository.create(commentListAllMock[0]);

    expect(result).toEqual(commentListAllMock[0]);
    expect(mysql.create).toHaveBeenCalledTimes(1);
    expect(mysql.create).toHaveBeenCalledWith(commentsQuery.create, {
      user_id: 9,
      flight_id: 5,
      content:
        "Mollit sit aliqua elit nostrud laborum laborum ipsum. Enim excepteur reprehenderit nulla voluptate cillum minim ullamco et id cupidatat dolore.",
      tags: "cupidatat,do",
    });
  });

  afterEach(() => {
    jest.restoreAllMocks(); // Restaura todos los mocks después de cada prueba
  });
});
