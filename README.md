# Versiones de herramientas
- NodeJS 18+
- Mysql

# Para levantar el proyecto
```env
PORT=3000

DB_PASSWORD=''
DB_HOST=
DB_PORT=
DB_USER=
DB_DATABASE=
```

## COMANDOS PARA INICIAR PROYECTO

```json
"dev": "nodemon --config nodemon.json",
"build": "tsc",
"test": "jest --silent false --coverage",
"start": "node build/src/index.js"
```

```bash
# restaurar base de datos con sql
npm run start
```