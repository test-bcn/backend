# build image from node18  alpine
FROM node@sha256:09faa7dabeae557cb6baff17be5b216fc4e3c9608aa04fe71695aad3d229a9c7

# set working directory
WORKDIR /usr/app

# copy package.json to working directory
COPY ./package.json ./

# install dependencies
RUN npm install

# copy all files to working directory
COPY ./ ./
COPY .env.prod .env

# build
RUN npm run build

# expose port
EXPOSE 80

# run command
CMD ["npm", "start"]